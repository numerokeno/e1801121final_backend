package fi.vamk.e1801121.final_backend;

import fi.vamk.e1801121.final_backend.controllers.StudentController;
import fi.vamk.e1801121.final_backend.entities.Student;
import fi.vamk.e1801121.final_backend.repositories.StudentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertThat;

@SpringBootTest
class FinalBackendApplicationTests {

	@Autowired
	StudentRepository studentRepository;
	@Autowired
	StudentController studentController;

	@Test
	void contextLoads() {

	}

	@Test
	public void AddToDatabse(){
		Student student = new Student(1,"Test","7A");
		studentRepository.save(student);
		studentRepository.delete(student);
	}

	@Test
	public void FindStudentById(){
		Student student2 = new Student(2,"Test","7A");
		studentRepository.save(student2);
		studentRepository.findById(2);
		studentRepository.delete(student2);
	}
}
