package fi.vamk.e1801121.final_backend.entities;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class Score {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // autoincrement the id for next record
    private int id;
    //@ManyToOne(cascade = CascadeType.REMOVE)
    @ManyToOne
    @JoinColumn(name = "studentid")
    private Student student;
    private Date rundate;
    private double rundistance;
    private String supervisor;

    public Score(){}

    public Score(int id, Student student, Date rundate, double rundistance, String supervisor) {
        this.id = id;
        this.student = student;
        this.rundate = rundate;
        this.rundistance = rundistance;
        this.supervisor = supervisor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Date getRundate() {
        return rundate;
    }

    public void setRundate(Date rundate) {
        this.rundate = rundate;
    }

    public double getrundistance() {
        return rundistance;
    }

    public void setrundistance(double rundistance) {
        this.rundistance = rundistance;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    @Override
    public String toString() {
        return "Result{" +
                "id=" + id +
                ", student=" + student +
                ", rundate=" + rundate +
                ", rundistance='" + rundistance + '\'' +
                ", supervisor='" + supervisor + '\'' +
                '}';
    }
}
