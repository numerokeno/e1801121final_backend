package fi.vamk.e1801121.final_backend.repositories;

import fi.vamk.e1801121.final_backend.entities.Score;
import fi.vamk.e1801121.final_backend.entities.Student;
import org.springframework.data.repository.CrudRepository;

public interface ScoreRepository extends CrudRepository<Score, Integer> {
    void deleteByStudent(Student student);
}