package fi.vamk.e1801121.final_backend.repositories;

import fi.vamk.e1801121.final_backend.entities.Student;
import org.springframework.data.repository.CrudRepository;


public interface StudentRepository extends CrudRepository<Student, Integer> {
}
