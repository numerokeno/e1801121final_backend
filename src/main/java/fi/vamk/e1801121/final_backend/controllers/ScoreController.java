package fi.vamk.e1801121.final_backend.controllers;

import fi.vamk.e1801121.final_backend.entities.Score;
import fi.vamk.e1801121.final_backend.repositories.ScoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class ScoreController {
    @Autowired
    ScoreRepository scoreRepository;

    //GET
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/scores")
    public Iterable<Score> getAll() {
        return scoreRepository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/score/{id}")
    public Optional<Score> get(@PathVariable("id") int id){
        return scoreRepository.findById(id);
    }

    //SAVE
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/score")
    public @ResponseBody
    Score create(@RequestBody Score item){
        return scoreRepository.save(item);
    }

    //PUT
    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping("/score")
    public @ResponseBody
    Score update(@RequestBody Score item){
        return scoreRepository.save(item);
    }

    //DELETE
    @CrossOrigin(origins = "http://localhost:3000")
    @DeleteMapping("score/{id}")
    void deleteRecord(@PathVariable int id){
        scoreRepository.deleteById(id);
    }

}

