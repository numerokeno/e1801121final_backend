package fi.vamk.e1801121.final_backend.controllers;

import fi.vamk.e1801121.final_backend.entities.Student;
import fi.vamk.e1801121.final_backend.repositories.ScoreRepository;
import fi.vamk.e1801121.final_backend.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class StudentController {
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    ScoreRepository scoreRepository;

    //GET
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/students")
    public Iterable<Student> getAll() {
        return studentRepository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/student/{id}")
    public Optional<Student> get(@PathVariable("id") int id){
        return studentRepository.findById(id);
    }

    //SAVE
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/student")
    public @ResponseBody Student create(@RequestBody Student item){
        return studentRepository.save(item);
    }

    //PUT
    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping("/student")
    public @ResponseBody Student update(@RequestBody Student item){
        return studentRepository.save(item);
    }


    //DELETE
    @CrossOrigin(origins = "http://localhost:3000")
    @DeleteMapping("student/{id}")
    @Transactional
    void deleteStudent(@PathVariable int id){
        //scoreRepository.deleteByStudent(new Student(id,"",""));
        studentRepository.deleteById(id);
    }
}
